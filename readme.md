# AEK React Boilerplate

This is an example AEK project featuring some of the latest additions to the AEK 2.0 family. It integrates with SimpleStore (with the localStorage plugin) and it has the basic AekReactRouter setup in the frontend code already setup. It also introduces aek-config, with default values for certain objects. It loads in custom React components and their associated `.styl` files (e.g. `/js/components/verticalSegment.jsx`).

It also contains some common CSS tweaks used to fix minor issues on small displays, some of which are optional (e.g. ListviewItem link icon placement, Banner padding). Optional improvements are applied via a custom class name, i.e. `<BannerHeader className='reduced'>Header</BannerHeader>`.

### How To Use

Run `aek create` from your command line terminal, but add `-b complete-project-template` to the end of it. A full example would be `aek create test-project -b complete-project-template`. Below is a list of the main files you will use in any AEK project.

#### `/js/main.jsx`

Container for the entire React view for the integration. Shows how the AekReactRouter works and demonstrates this by showing a single page within it.

#### `/js/pages/index.jsx`

Main page shown when you run this boilerplate for the first time. It calls a simple web service and displays the results in a Listview.

#### `/screens/includes.actions.twig`

This is your basic web service container, it uses Web Service Endpoints as defined in App Manager (under App Settings > Web Service Endpoints) or third-party URLs to fetch data from outside the AEK. Included is an example calendar views service call, which all client apps should have by default.

#### `/js/scripts/generic-store.es6.js`

SimpleStore template with basic logic present, a console logger plugin and a localStorage plugin. Feel free to name this file, but make sure the associated imports are also updated.

#### `/js/scripts/generic-utils.js`

Common JavaScript utils, such as a simple colour-to-hex code lookup, or a JavaScript object validation method. Feel free to name this file, but make sure the associated imports are also updated.

#### `/css/main.styl`

This is where custom CSS goes. This file already contains some optional CSS that you might find useful, as well as some fixes that have been applied to projects across the board.

-----------

# Technical details about this boilerplate

### Modifying Source Code

Source code can be modified in the `/src` directory.

#### CSS
Anything at the root of the css directory `/src/css/[name].*` will be treated as an entry point for the AEK CSS buildtool. This will process the source code by transpiling any less `[name].less` and stylus `[name].styl`. The output of this, along with any `[name].css` files will be passed on to autoprefixer post processing (this adds vendor prefixes to css properties). In development, the result is supplemented with source mapping, in production, it is minified. The output file will be available at `[publicPath]/css/[name].css`

The boilerplate contains a default stylesheet that uses the aek-css framework to provide a number of css components. You can configure colours, etc in `/aek-css-config.styl`

#### JS
Anything at the root of the js directory `/src/js/[name].*` will be treated as an entry point for AEK webpack buildtool. This will process source code by transpiling any `[name].es6.js` or `[name].jsx` files with BabelJS, and any `*.coffee` files through CoffeeScript. The webpack buildtool can also process less,stylus and css as css modules which are injected into the document with a styl-loader. Again, source maps are applied and production assets are minified. The output file will be available at `[publicPath]/js/[name].js`

The boilerplate includes some basic `.jsx` code that demonstrates some of the available react components.

_note that [publicPath] will be `/public` in development but will be very different in production_

#### Screens
Everything at the root of the screens directory `/src/screens/*.ect` will be treated as an ECT template. The output from the ECT process will provide either raw HTML (for disconnected screens), or the Twig code that is sent to the server (for connected screens).

"Disconnected" screens are served locally. "Connected" screens can include twig server-side processing and are served via the CampusM servers. To "connect" a screen set a `connected:true` property in the `@configure` section in the screen code.

### Other Public assets
Other public assets such as images or fonts can be added to the `/public` directory

### Runserver Config
You can configure aspects of the local server in `/runserver.yaml` - this allows you to override, customise or add the menu options and other homescreen properties.
