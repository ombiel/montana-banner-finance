const SimpleStore = require('-aek/simple-store');
const request = require('-aek/request');
const moment = require('moment');
const _ = require('-aek/utils');
const Notifier = require('-aek/notifier');

const Config = require('../modules/config');

// these can be disabled by commenting them out of the plugins array in the constructor()
const loggerPlugin = require('-aek/simple-store/plugins/logger');
const localStoragePlugin = require('-aek/simple-store/plugins/localstorage');

// i.e. 'aek2-test-project-key', this is defined as a JavaScript string explicitly in the JSON Config object
const storageKey = Config.storageKey;

// this project comes with an implementation of SimpleStore, but this isn't necessary to manage state in React; it's just what we recommend
// https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/simple-store
// NOTE - with localStoragePlugin enabled, any changes to initialState will only be reflected by clearing your browser's localStorage (or by logging out and back in)
class GenericStore extends SimpleStore {
  constructor() {
    let plugins = [];
    // comment out either of these to deactivate them
    plugins.push(loggerPlugin());
    plugins.push(localStoragePlugin(storageKey));

    super({
      initialState: {
        balance: {
          balance: null,
          balanceError: null,
          $$$balanceLoading: true
        }
      },
      plugins: plugins
    });

    // you can modify state values here if you absolutely have to, i.e. if you're checking for something on startup
  }

  // you'll need one dispatch and one fetch method for each service, or group of service calls (depending on implementation)
  dispatchBalance(ctx, balance, isError, errorMessage, loading) {
    ctx.dispatch({
      name: 'BALANCE',
      error: isError,
      extend:{
        balance: balance,
        lastUpdated: moment().format(),
        balanceError: errorMessage,
        $$$balanceLoading: loading
      }
    });
  }

  fetchBalance() {
    var ctx = this.context({ group:"BALANCE", path:"balance" });
    this.dispatchBalance(ctx, null, false, null, true);
    request.action("get-balance").end((err, res) => {
      if(!err && res && res.body !== null && !res.body.error) {
        this.dispatchBalance(ctx, JSON.parse(res.body.json)[0].balance, false, null, false);
      } else {
        this.dispatchBalance(ctx, null, true, res.body, false);
      }
    });
  }

  // message takes plaintext only; HTML will be rendered as such
  // more examples of default parameters values
  genericNotifier(title, message, dismissable, level, autoDismiss = 0, clear = true) {
    if(clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      message: message,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

  // children allows you to pass HTML to the Notifier to be displayed
  notifierWithChildren(title, children, dismissable, level, autoDismiss = 0, clear = true) {
    if(clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      children: children,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

}

module.exports = new GenericStore();
