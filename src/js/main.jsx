const React = window.React = require('react');
const reactRender = require('-aek/react/utils/react-render');
const { VBox, Panel, CBox } = require('-components/layout');
const { AekReactRouter, RouterView } = require('-components/router');
const Container = require('-components/container');
const { BannerHeader } = require('-components/header');
const {Message} = require("@ombiel/aek-lib/react/components/message");
let moment = require("moment");
const { BasicSegment } = require('-components/segment');
var Button = require("@ombiel/aek-lib/react/components/button");

const Config = require('./modules/config');
const GenericStore = require('./scripts/generic-store');

let Screen = React.createClass({
  componentDidMount:function() {
    GenericStore.fetchBalance();
    // this is required to trigger a rerender on SimpleStore update
    // only works thanks to React's fast virtual DOM; would impact performance otherwise
    GenericStore.on('change', () => {
      this.forceUpdate();
    });

    // endsWith fallback for IE - https://caniuse.com/#search=endswith
    // we also have a parallel implementation in GenericUtils if you want to avoid this
    if(!String.prototype.endsWith) {
      String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
      };
    }
  },

  componentWillUnmount:function() {
    GenericStore.off('change');
  },

  render:function() {

    let theme = Config.theme.primary;
    let balance = GenericStore.get("balance.balance");
    let lastUpdated = GenericStore.get("balance.lastUpdated");
    let loading = GenericStore.get("balance.$$$balanceLoading");

    let style = null;

    if (balance < 0){
      style = {color:"red"};
    }
    else {
      style = {color:"green"};
    }

    return (
      <Container>
        <VBox>
          <BannerHeader theme={ theme } key='index-header' data-flex={0}>{ Config.language.pageName }</BannerHeader>
          <BasicSegment key='message' data-flex={0}>
            <Message theme="alt">{ Config.language.lastUpdatedText}{moment(lastUpdated).format('LL') }</Message>
          </BasicSegment>
          <Panel>
            <BasicSegment style={{height:"100%"}} loading={loading}>
              <CBox>
                <h1 className="balanceText" style={style}>{Config.currency_prefix}{balance}</h1>
              </CBox>
            </BasicSegment>
          </Panel>
          <div  data-flex={0} key='footer'>
            <BasicSegment>
              <Button size="big" variation="alt" fluid href={Config.moreInfoURL}>{Config.language.buttonText}</Button>
            </BasicSegment>
          </div>
        </VBox>
      </Container>
    );
  }
});

reactRender(<Screen />);
